# `mcc`: Martin's `C--` Compiler

## Questions

### Question 1:
**En supposant que `gcc` ne vous autorise pas à mélanger les blocs de type code
et le blocs de type texte, est-il nécessaire de faire un parcours spécifique
pour associer une adresse (un label) à chaque chaîne de caractères du programme ?
Expliquez.**

Non, ce n'est pas nécessaire: il suffit lors du parcours de l'arbre de syntaxe
abstraite de traiter les chaînes de caractères qui sont déclarées dans le code:
on assigne à chaque de caractères un identifiant unique, et on stocke
l'identifiant de la chaîne et son contenu dans une collection de notre
environnement OCaml. On peut ici prendre une `list ref` de manière à ce que
l'environnement finisse par contenir l'ensemble des chaînes de caractères avec
leur identifiant. Ensuite, une fois le parcours de compilation terminé, on a
écrit le code correspondant à toutes les déclarations de la source `C--` dans
un bloc `code` dans le fichier du code assembleur. On peut alors commencer à
écrire un bloc `text`, et y déclarer toutes les chaînes dont nous avons
connaissance.

Par exemple, à la fin du parcours de l'arbre de syntaxe abstraite correspondant
au code source suivant:
```C
printf("Hello, ");
printf("World\n");
```
on aura obtenu le code compilé suivant (simplifié pour l'exemple):
```assembly
    leaq  .label_Hello(%rip), %rsi
    call  printf
    leaq  .label_World(%rip), %rsi
    call  printf
```
et on aura dans notre environnement la liste de chaînes suivante:
```
[(".label_hello", "Hello, "); (".label_World", "World\n")]
```
ce qui nous permet donc de compiler après la fin du bloc `text` le bloc `data`
suivant:
```assembly
.label_Hello:
    .string "Hello, "
.label_World:
    .string "World\n"
```

### Question 2:
**Lors de la partie 1, vous avez dû gérer la portée lexicale des variables
locales. Pour cela, vous avez utilisé un environnement qui associait à chaque
variable locale son adresse sur la pile. Il existe principalement deux types
OCaml pour un tel environnement:**
```ocaml
type env = (var * adress) list
```
**ou bien**
```ocaml
type env = (var * adress) list ref
```
**Expliquez la différence entre ces deux types et les conséquences sur le
compilateur. Selon vous, y-a-til un type meilleur qu'un autre ? Pourquoi ?**

Le premier type est simplement une liste des couples `(variable, adresse)` tandis
que le second type est une référence vers une liste de couples. La différence
est donc de taille: si à chaque nouveau `CBLOCK` on crée un nouvel environnement
en ajoutant les variables déclarées dans le `CBLOCK`, le premier environnement
ne contiendra la déclaration des variables *que dans les noeuds fils du `CBLOCK`*,
tandis que le deuxième environnement contiendra la déclaration de ces variables
*jusqu'à la fin du parcours de l'arbre de syntaxe abstraite*.

Le premier type d'environnement va donc *nativement* permettre de gérer la portée
des variables, puisqu'une variable n'existera dans l'environnement que dans le
bloc de code / la fonction / le *handler* d'exception où elle a été déclarée,
tandis que le deuxième type nécessitera de supprimer *manuellement* la variable
déclarée en début de bloc à la fin de la compilation d'un bloc pour éviter d'avoir
dans l'environment des variables hors-champ.

Le premier type est donc meilleur pour gérer les variables locales. Cependant,
le second type peut-être meilleur dans un cas précis: pour garder en mémoire
des variables globales, ou des chaînes de caractères.

En effet, dans le cas où on parcourt le graphe et qu'on garde en mémoire les
chaînes de caractères lues dans le code source, il est plus pratique que
la liste des chaînes de caractères soit partagée: on peut ainsi éviter les
doublons quand on rencontre une chaîne déjà déclarée (et éviter d'ajouter
10 fois la chaîne `"ERROR"` à la section `data` quand on compile un code source
contenant 10 fois `printf("ERROR")`).



### Question 3
**Dans la partie 2, il faut être capable de comparer deux exceptions afin de
savoir si l'exception courante est rattrapée par une clause `catch`. Comment
avez-vous implémenté cette comparaison ?**

Afin de gérer les exceptions, j'utilise les registres $r10$ et $r11$: $r10$
contient un nombre correspondant à un exception ($0$ quand il n'y a pas
d'exception, $-1$ pour un `return`, un nombre positif sinon), et $r11$ la valeur
associée à l'exception renvoyée par le code. Etant donné que $r10$ et $r11$
sont caller-save, on ne viole pas l'ABI Linux en les modifiant dans une fonction.

Mon compilateur garde dans l'environnement un référence vers une liste connaissant
toutes les exceptions (pouvant être rattrapée ou lancée), identifiées de manière
unique. La première exception rencontrée dans l'AST (indifféremment de si elle
a été rencontrée dans une clause `catch` ou dans un `throw`) a donc l'identifiant
$1$, la seconde $2$, etc. Ainsi, en supposant que `A` soit la première exception
dans la source, le code suivant:
```C++
throw A(42);
```
est compilé en:
```assembly
    movq $1, %r10
    movq $42, %r11
```

Il suffit alors de comparer le contenu de $r10$ avec l'identifiant de l'exception
attendue par la clause `catch`: s'ils sont égaux, alors l'exception est attrapée
par la clause, sinon, on saute à la clause suivante, ou au `finally` quand il
n'y a plus de clause restante.

Le code suivant:
```C++
try {
    throw A(42);
} catch (B x) {
    // code catch B
} catch (A x) {
    // code catch A
} finally {
    // code finally
}
```
est donc compilé (de manière simplifiée pour l'exemple) en
```assembly
.try:
    movq $1, %r10;    # le bloc de code
    movq $42, %r11;   # correspondant
    jmp .catchB;      # au throw A(42)
.catchB:
    cmp %r10, $2
    jne .catchA
    movq $0, %r10
    # code catch B
    jmp .finally
.catchA:
    cmp %r10, $1
    jne .finally
    movq $0, %r10
    # code catch A
    jmp .finally
.finally:
    # code finally
.end:
```


### Question 4
**Selon la sémantique, est-ce que la clause `finally` est toujours exécutée ?
Si oui, alors expliquez informellement pourquoi. Sinon, donnez un contre-exemple
et expliquez pourquoi la cause ne sera pas toujours exécutée.**

La règle
$$
\frac{\rho, \mu \vdash c \Rightarrow \text{ret } n, \mu^\prime}
{\rho, \mu \vdash c :: L \Rightarrow \text{ret } n, \mu^\prime} (SeqRet)
$$
permet d'affirmer que lors d'une exécution séquentielle d'une liste de code,
on interrompt l'exécution au premier paquet de retour.

Or la règle
$$
\frac{\rho[x_1 \mapsto a_1], \mu^\prime[a_1 \mapsto n_1] \vdash [c_1; c^\prime] \Rightarrow res}
{\rho, \mu^\prime, \text{throw}(E, n) \vdash (E_1, x_1, c_1) :: HL \text{ finally } c^\prime \Rightarrow ref} (\text{catch}Oui)
$$
indique que lorsqu'on trouve un handler correspondant à une exception, on doit
renvoyer le paquet de retour résultant de l'exécution du code formé par la
liste $[c_1; c^\prime]$ dans l'environnement $\rho[x_1 \mapsto a_1]$ avec une
mémoire $\mu^\prime[a_1 \mapsto n_1]$, où $c_1$ est le code du handler et
$c^\prime$ le code du $\text{finally}$.

Or si le $\text{catch}$ renvoie directement un paquet de retour, alors
$[c_1; c^\prime]$ va renvoyer un paquet de retour sans qu'on ait exécuté le
code du `finally`, mais on peut quand même applique $(\text{catch}Oui)$.

Donc dans le code suivant:
```C++
int f() {
  try {
      throw A(1);
  } catch (A n) {
    return n;
  } finally {
    printf("ERROR!\n");
  }
}
```
le code du `finally` ne sera jamais exécuté, vu que l'exécution du code sera
interrompue dans le `catch`, qui renverra directement la valeur de $n$, donc $1$.



### Question 5
**Est-ce que votre stratégie de compilation pour les exceptions vous a demandé
de modifier le code que vous aviez écrit pour la partie 1 ? Si oui, quelles
parties sont concernées ? Pourquoi ? Est-il possible de faire la partie 2 sans
impacter le code de la partie 1 ?**

Il a été nécessaire de modifier:

* l'environnement, pour stocker deux variables supplémentaires:
    - une liste partagée des exceptions connues dans le programme
    - le label correspondant au `finally` (et possiblement au premier `catch`)
      correspondant au bloc `try` dans lequel se trouve le code qu'on compile,
      ou `(None, None)` sinon.
* la façon dont étaient gérés les `return`, car on ne peut plus faire de
  `leave ret` de manière inconditionelle vu qu'il faut parfois aller exécuter
  le code du `finally` correspondant
* la manière de gérer les appels de fonction, afin de prendre en compte
  si une fonction a renvoyé une exception ou non, et la *relancer* (sauf pour
  les fonctions de la `libc`).
* la *template* de la définition d'une fonction, afin d'initialiser $r10$ et
  $r11$ à $0$ avant toute autre exécution.

Il n'est à mon avis pas possible de ne pas modifier au moins la partie
correspondant au `return`, sous peine de ne probablement pas respecter la règle
$$
\frac{\rho, \mu \vdash c \Rightarrow \text{ret } n, \mu^\prime \quad\quad \rho, \mu^\prime \vdash c^\prime \Rightarrow \mu^{\prime\prime}}
{\rho, \mu \vdash \text{CTRY}(c, HL, \text{Some } c^\prime) \Rightarrow \text{ret } n, \mu^{\prime\prime}} (\text{CTRY}Ret)
$$
puisqu'il faut désormais prendre en compte le fait qu'un `return` ne signifie
plus la fin de la fonction, mais l'exécution du code du `finally` avant de renvoyer
une valeur lorsqu'on est dans un bloc `try`.


### Question 6
**Selon vous, quels sont les avantages à utiliser des exceptions en `C--` ?**

L'avantage des exceptions est qu'elles permettent à un code de gérer ses erreurs
de manière plus optimisée (par le compilateur), et de permettre une syntaxe plus
claire:
```C
if (fonction_1_qui_fait_une_erreur()) {
    printf("Erreur !");
}

if (fonction_2_qui_fait_une_erreur()) {
    printf("Erreur !");
}

if (fonction_3_qui_fait_une_erreur()) {
    printf("Erreur !");
}
```
devient plus lisible avec des exceptions:
```C++
try {
    fonction_1_qui_fait_une_erreur();
    fonction_2_qui_fait_une_erreur();
    fonction_3_qui_fait_une_erreur();
} catch (Erreur x) {
    printf("Erreur !");
}
```




## Stratégie de compilation

### Environnement

Mon environnement contient les variables supplémentaires:

* la liste (partagée via une référence) de toutes les exceptions dans le
  programme, afin de leur attribuer un identifiant unique, dans la variable
  `environnement.exception`
* le couple `(label_handler, label_finally)`, de type `string option * string option`
  qui permet de connaître, si on est dans un bloc `try`, le label correspondant
  à la première clause `catch` (s'il y en a une) et celui correspondant au
  `finally` de ce bloc de code.

Lorsqu'on entre dans un nouveau bloc `try`, on copie l'ancien environnement
en modifiant son couple de handlers (à l'aide de la méthode
`environnement#with_handler`) de manière à ce que les labels du bloc soient limités
au code contenu à l'intérieur du bloc.



### Cas de base

### `try`/`catch`/`except`

Pour compiler un bloc `try/catch/except`, le compilateur `mcc`:

* sauvegarde les registres d'exception ($r10$ et $r11$) et les initialise
  à $0$: cela permet d'exécuter le code du bloc `try` dans un contexte sans
  exception
* crée de nouveaux labels, et compile le bloc `try` avec un environnement
  modifié pour prendre en compte les labels du premier *handler* et
  du `finally`
* compile le code de chaque *handler*, en suivant la logique expliquée dans
  la question $3$, en ajoutant un `label` avant chaque bloc. A la fin de
  chaque handler, $r10$ et $r11$ sont remis à zéro, puisque l'exception a été
  attrapée avec succès.
* ajoute un label pour le code du `finally`, puis compile le code du `finally`
* ajoute le code permettant de faire le choix suivant de traiter si l'exception
  a été rattrapée ? si oui, ne rien faire; sinon, la *relancer* (i.e., sauter
  au *handler* le  plus proche, ou bien `leave ret` si on n'est pas dans un bloc try).
* restaure $r10$ et $r11$ à leur état initial

Quand on lance une exception, le programme va sauter de *handler* en *handler*
jusqu'à trouver le *handler* idoine, ou exécuter le code du `finally` sinon.


### `throw`

On compile le code
```C++
throw A(expr);
```
de la manière suivante:

* on met dans $r10$ l'identifiant de l'exception $A$
* on met dans $r11$ le résultat de l'expression $expr$
* on saute au *handler* le plus proche de manière inconditionelle

Dans le cas particulier où le `throw` ne se trouve pas dans un `try`, alors on
ne peut pas aller au *handler* le plus proche: à la place, on met $1$ dans $rax$
(pour signaler une erreur, en particulier dans le cas où on est dans `main`),
puis on effectue un `leave ret` comme si on compilait un `return`.


### `return`

Un `return` ne peut plus *simplement* interrompre l'exécution séquentielle du
programme et retourner à l'adresse appelante; il faut désormais dans certains
cas exécuter le code du `finally` si on se trouve dans un bloc `try`.

Pour ce faire, on remplace la compilation inconditionelle d'un
`return` en `leave ret` par une compilation qui dépend de l'environnement:

* si on n'est pas dans un bloc `try`, ou si on est dans un bloc `try` sans
  `finally`, alors on peut simplement `leave ret`
* si on est dans un bloc `try`, alors on va faire comme on lançait une exception,
  sauf que c'est une exception spéciale ($Return$), dont l'identifiant est $-1$.
  Ainsi, on met $-1$ dans $r10$, et la valeur de retour dans $r11$, et on saute
  dans le `finally`. A la fin du `finally`, si $r10$ vaut $-1$, alors selon si
  l'on se trouve dans un bloc `try` ou non, on va soit `leave ret` soit sauter
  au `finally` suivant. Etant donné que la valeur de retour reste dans $r11$,
  elle n'est pas modifiée même si la mémoire est modifiée par les différents
  blocs `finally`. Juste avant le `leave ret`, on déplace la valeur de retour
  de $r11$ à $rax$, puis on remet $r10$ et $r11$ à $0$.


### Appels de fonctions

L'appel de fonction est quasiment le même, sauf que:

* on sauvegarde $r10$ et $r11$ avant d'appeler la fonction
* on appelle la fonction
* si c'est une fonction non-déclarée (probablement de la `libc`), on ne fait
  rien de plus que restaurer $r10$ et $r11$
* sinon, alors il faut regarder si la fonction a renvoyé une exception en
  comparant $r10$ avec $0$:

  - si $r10 = 0$, alors c'est bon, il n'y a pas eu d'exception, on peut
    restaurer $r10$ et $r11$ et continuer l'exécution du programme
  - si $r10 > 0$, alors il y a eu une exception: on saute directement
    au *handler* ou au `finally` si on est dans un bloc `try`; sinon, on
    continue de renvoyer cette valeur à la fonction appelante avec un
    simple `leave ret`


### Autres stratégies et améliorations possibles

* J'utilisais dans un premier temps des labels pour donner un identifiant
  aux exceptions, plutôt que de le faire directement dans le code OCaml du
  compilateur. Il fallait alors charger les adresses avec `leaq` dans des
  registres temporaires, et faire plus d'opérations que nécessaires, c'est
  pourquoi j'ai repensé le compilateur pour assigner des identifiants directement
  à la compilation.
* On pourrait, au lieu de sauter au premier *handler* et ensuite de sauter de
  *handler* en *handler*, sauter directement au bon *handler* pour une exception,
  ou au `finally` le cas échéant; ça n'est pas possible de faire ça quand on
  lance une exception après un appel de fonction, mais ça l'est lorsqu'on lance
  une exception avec `throw`.
* En l'état, le compilateur ajoute un certain coùt aux blocs `try`, liés à la
  sauvegarde de $r10$ et $r11$ sur la pile. Il est probablement possible de
  s'en passer, par exemple en utilisant %r10 en tant que pointeur vers
  une adresse contenant un tableau de contextes, etc.


### Tests

Les tests s'exécutent avec Python: dans le répertoire du projet,
```
python3 -m unittest discover -v
```

Ils utilisent les modules externes [`fs`](https://github.com/PyFilesystem/pyfilesystem2) et
[`sh`](https://github.com/amoffat/sh) et [`pygments`](http://pygments.org/)
qui sont inclus dans le dossier `tests/lib` pour rendre l'utilisation des tests
plus simples.


La compilation et les tests unitaires ont été lancés sur les machines suivantes:

- Linux 01 4.4.0-101-generic #124-Ubuntu SMP Fri Nov 10 18:29:59 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux (01.dptinfo.ens-cachan.fr)
- Linux spin5 4.13.11-1-ARCH #1 SMP PREEMPT Thu Nov 2 10:25:56 CET 2017 x86_64 GNU/Linux (personal laptop)
- Linux runner-e11ae361-project-4601520-concurrent-0 4.13.9-coreos #1 SMP Thu Oct 26 03:21:00 UTC 2017 x86_64 Linux (GitLab-CI executor)
