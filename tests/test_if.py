# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestIf(CompilerTestCase):

    def test_if_integer(self):
        self._test_program(
            """
            int main(int argc) {
                int i;
                i = 1;
                if (i) {
                    return 0;
                } else {
                    return 1;
                }
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_if_cmp(self):
        self._test_program(
            """
            int main(int argc) {
                int i;
                i = 1;
                if (argc <= 2) {
                    return 0;
                } else {
                    return 1;
                }
            }
            """,
            Call([], 0, b"", b""),
            Call(["abc"], 0, b"", b""),
            Call(["abc", "def"], 1, b"", b""),
        )
