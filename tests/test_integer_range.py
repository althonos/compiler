# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from . import config
from .base import CompilerTestCase, Call


class TestIntegerRange(CompilerTestCase):

    maximum = (1 << config.getint('tests', 'integer-range', fallback=64)) - 1

    def test_constant_range(self):
        self._test_program(
            """
            int main() {{
                int x;

                x = {};
                printf("%llu", x);

                return x;
            }}
            """.format(self.maximum + 5),
            Call([], 4, b"4", b""),
        )

    def test_add_range(self):
        self._test_program(
            """
            int main() {{
                int i;
                i = {} + 2;
                printf("%ld", i);
                return i;
            }}
            """.format(self.maximum),
            Call([], 1, b"1", b"")
        )

    def test_postinc_range_var(self):
        self._test_program(
            """
            int main() {{
                int i, j;

                i = {};
                j = i++;

                printf("%lu\\n", i);
                printf("%lu\\n", j);

                return 0;
            }}
            """.format(self.maximum),
            Call([], 0, "0\n{}\n".format(self.maximum).encode('utf-8'), b"")
        )

    def test_postdec_range_var(self):
        self._test_program(
            """
            int main() {{
                int i, j;

                i = 0;
                j = i--;

                printf("%llu\\n", i);
                printf("%llu\\n", j);

                return 0;
            }}
            """.format(self.maximum),
            Call([], 0, "{}\n0\n".format(self.maximum).encode('utf-8'), b"")
        )

    def test_preinc_range_var(self):
        self._test_program(
            """
            int main() {{
                int i, j;

                i = {};
                j = ++i;

                printf("%llu\\n", i);
                printf("%llu\\n", j);

                return 0;
            }}
            """.format(self.maximum),
            Call([], 0, b"0\n0\n", b"")
        )

    def test_negation_range(self):
        self._test_program(
            """
            int main() {{
                int i;
                i = {};
                printf("%llu", ~i);
                return ~i;
            }}
            """.format(self.maximum),
            Call([], 0, b"0", b"")
        )

    def test_minus_range(self):
        self._test_program(
            """
            int main() {{
                int i;
                i = {};
                printf("%llu", -i);

                return -i;
            }}
            """.format(self.maximum),
            Call([], 1, b"1", b"")
        )
