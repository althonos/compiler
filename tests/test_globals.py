# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestGlobals(CompilerTestCase):

    def test_set_global_in_main(self):
        self._test_program(
            """
            int i;

            int main() {
                i = 2;
                return i;
            }
            """,
            Call([], 2, b"", b""),
        )

    def test_set_global_in_aux(self):
        self._test_program(
            """
            int i;

            int increase_i() {
                i = i+1;
                return;
            }

            int main() {
                i = 0;
                increase_i();
                increase_i();
                increase_i();
                return i;
            }
            """,
            Call([], 3, b"", b""),
        )

    def test_global_array(self):
        self._test_program(
            """
            int array;

            int show_array() {
                int i;
                for (i=3; i != 0; printf("%i", array[--i]));
            }

            int main() {
                int i;

                array = malloc(8*3);

                for (i=0; i < 3; printf("%i", array[i++] = i));
                for (i=0; i < 3; printf("%i", array[i++]));
                show_array();

                return 0;
            }
            """,
            Call([], 0, b"012012210", b"")
        )
