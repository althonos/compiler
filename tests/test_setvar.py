# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestSetVar(CompilerTestCase):

    def test_many_locals(self):
        self._test_program(
            """
            int main() {
                int a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;
                a=1;b=1;c=1;d=1;e=1;f=1;g=1;h=1;i=1;j=1;k=1;l=1;m=1;n=1;o=1;
                p=1;q=1;r=1;s=1;t=1;u=1;v=1;w=1;x=1;y=1;z=1;
                return a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p+q+r+s+t+u+v+w+x+y+z;
            }
            """,
            Call([], 26, b'', b'')
        )

    def test_setvar_single(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 0;
                return i;
            }
            """,
            Call([], 0, b"", b""),
        )

    def test_setvar_as_expr(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 0;
                i = 2;
                return i=12;
            }
            """,
            Call([], 12, b"", b""),
        )

    def test_setvar_array(self):
        self._test_program(
            """
            int main() {
                int* i;
                i = malloc(8*2);
                i[0] = 12;
                i[1] = 10;
                return i[0]+i[1];
            }
            """,
            Call([], 22, b"", b""),
        )
