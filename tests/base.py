# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import time
import textwrap
import tempfile
import unittest
import collections
import pygments

import fs
import fs.tempfs
import fs.copy
import sh

from . import config, projfs, lexer, fmter

Call = collections.namedtuple('Call', ['args', 'retcode', 'stdout', 'stderr'])


class CompilerTestCase(unittest.TestCase):

    @classmethod
    def highlight(cls, line):
        return pygments.highlight(line, lexer, fmter)

    @classmethod
    def setUpClass(cls):
        cls.projfs = fs.open_fs('.')

    def setUp(self):
        self.testfs = fs.tempfs.TempFS()

    def tearDown(self):
        self.testfs.close()

    def _test_program(self, program, *calls):
        try:
            program = textwrap.dedent(program)
            mcc = sh.Command(projfs.getsyspath('/mcc'))

            self.testfs.settext('/test.c', program)
            with self.testfs.openbin('test.s', 'wb') as f:
                mcc(self.testfs.getsyspath('test.c'), E=True, _out=f)

            sh.gcc(
                self.testfs.getsyspath('test.s'), "-ggdb",
                o=self.testfs.getsyspath('test'),
                l="pthread",
            )

            prog = sh.Command(self.testfs.getsyspath('./test'))

            for call in calls:
                result = prog(*call.args, _ok_code=range(255))
                self.assertEqual(call.stdout, result.stdout, "stdouts differ")
                self.assertEqual(call.stderr, result.stderr, "stdouts differs")
                self.assertEqual(call.retcode, result.exit_code, "retcodes differ")

        except Exception:
            try:
                # show the generated assembler code
                if config.getboolean('tests', 'show-assembler', fallback=True):
                    for i, x in enumerate(self.testfs.open('test.s')):
                        print(self.highlight("{:03}:\t{}".format(i+1, x)), end='')
                # write the generated assembler code
                debug_path = config.get('tests', 'debug-path', fallback="")            
                with open(debug_path, 'wb') as dst:
                    with self.testfs.openbin('test.s') as src:
                        dst.write(src.read())
            except Exception:
                pass
            raise
