# coding: utf-8
"""Compilers tests
==================

Requirements
------------

* `pygments <http://pygments.org/>`_: colorfully report the generated
  assembler code of failed tests.
* `sh <https://github.com/amoffat/sh>`_: invoke shell commands in
  a graceful manner
* `fs <https://github.com/PyFilesystem/pyfilesystem2>`_: manage the
  filesystem of the project as an object.
* `green <https://github.com/CleanCut/green>`_: (*optional*) pretty test
  reporting

License
-------

Copyright 2017 Martin Larralde <martin.larralde@ens-cachan.fr>

This Python module ('tests') is licensed under the MIT license.
The C sources in the 'ext' folder are not part of this module, but
are considered to be part of Public Domain.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions: The above copyright notice and
this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINRE_INCFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


import os
import sys
import configparser

# Add local lib folder to $PYTHONPATH
sys.path.insert(0, os.path.abspath(os.path.join(__file__, "..", "lib")))

import sh
import fs
import pygments.lexers
import pygments.formatters

# project global variables
projfs = fs.open_fs(os.path.join(__file__, os.pardir, os.pardir))
config = configparser.ConfigParser()

# read the configuration from the setup.cfg file
if projfs.isfile('setup.cfg'):
    config.read_string(projfs.gettext('setup.cfg'))

# compile the latest mcc
sh.make(directory=projfs.getsyspath('/'))

# formatting tools
lexer = pygments.lexers.get_lexer_by_name('Gas')
fmter = pygments.formatters.get_formatter_by_name('terminal256')
