# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from .base import CompilerTestCase, Call


class TestPrint(CompilerTestCase):

    def test_hello_world(self):
        self._test_program(
            """
            int main() {
                printf("Hello, World!\\n");
                return 0;
            }
            """,
            Call([], 0, b"Hello, World!\n", b""),
        )

    def test_print_char_array_as_string(self):
        self._test_program(
            """


            int main() {
                int i;
                char* msg;

                msg = malloc(14*8);
                msg[0] = 72; msg[1] = 101; msg[2] = 108; msg[3] = 108;
                msg[4] = 111; msg[5] = 44; msg[6] = 32; msg[7] = 119;
                msg[8] = 111; msg[9] = 114; msg[10] = 108; msg[11] = 100;
                msg[12] = 33;

                for (i = 0; i < 13; i++) printf("%c", msg[i]); fflush(stdout);
                return 0;
            }
            """,
            Call([], 0, b"Hello, world!", b"")
        )
