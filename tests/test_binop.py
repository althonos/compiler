# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from . import config
from .base import CompilerTestCase, Call


class TestSetArray(CompilerTestCase):

    def test_return_value(self):
        self._test_program(
            """
            int add(int x, int y) {
                return x+y;
            }

            int main() {
                int* i; int j;
                i = malloc(16); i[0] = 1; i[1] = 2;

                j = i[add(4, -3)] = 8*4-7+3*1;
                printf("[%i, %i]", 1, 28);
                return j;
            }
            """,
            Call([], 28, b"[1, 28]", b"")
        )



class TestBinOp(CompilerTestCase):

    def test_add_const_to_const(self):
        self._test_program(
            """
            int main() {
                return 1+2;
            }
            """,
            Call([], 3, b"", b""),
        )

    def test_add_var_to_const(self):
        self._test_program(
            """
            int main() {
                int i;
                i = 5;
                return i+2;
            }
            """,
            Call([], 7, b"", b""),
        )

    def test_add_var_to_var(self):
        self._test_program(
            """
            int main() {
                int i, j;
                i = 5;
                j = 3;
                return i+j;
            }
            """,
            Call([], 8, b"", b""),
        )

    def test_sub_var_to_var(self):
        self._test_program(
            """
            int main() {
                int i, j;
                i = 1;
                j = 2;
                return j-i;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_mul_var_to_var(self):
        self._test_program(
            """
            int main() {
                int a;
                int b;
                a = 3;
                b = 2;
                return a*b;
            }
            """,
            Call([], 6, b"", b""),
        )

    def test_div_var_to_var(self):
        self._test_program(
            """
            int main() {
                int a;
                int b;
                a = 16;
                b = 3;
                return a/3;
            }
            """,
            Call([], 5, b"", b""),
        )

    def test_mod_var_to_var(self):
        self._test_program(
            """
            int main() {
                int a;
                int b;
                a = 10;
                b = 3;
                return a%3;
            }
            """,
            Call([], 1, b"", b""),
        )

    def test_neg_mod(self):
        self._test_program(
            """
            int main() {
                int a;
                int b;
                a = -3;
                b = 4;
                printf("%i", a%b);
                return 0;
            }
            """,
            Call([], 0, b"-3", b""),
        )

    def test_neg_div(self):
        self._test_program(
            """
            int main() {
                int a;
                int b;
                a = -3;
                b = 2;
                printf("%i\\n", a/b);
                printf("%i\\n", (-a)/b);
                return 0;
            }
            """,
            Call([], 0, b"-1\n1\n", b""),
        )

    def test_many_ops(self):
        self._test_program(
            """
            int main() {
                int i, j, k;
                i = 3;
                j = 10;
                k = 2;
                return j+i*(-k);
            }
            """,
            Call([], 4, b"", b"")
        )
