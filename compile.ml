(*
 * Copyright 2017 Martin Larralde <martin.larralde@ens-cachan.fr>
 *
 * This file ('compile.ml') is licensed under the MIT license.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions: The above copyright notice and
 * this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINRE_INCFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *)

open Cparse
open Genlab
open Printf

(* a label in the assembly code *)
type label_t =
  | LBL_STR       (* a global string label *)
  | LBL_IF        (* a label at the start of the `if` block (DEBUG, not needed) *)
  | LBL_THEN      (* a label at the start of the `if` branch (DEBUG, not needed) *)
  | LBL_ELSE      (* a label at the start of the `else` branch *)
  | LBL_FI        (* a label after an if/else block *)
  | LBL_WHILE     (* a label at the start of a while block *)
  | LBL_DO        (* a label at the start of the loop content (DEBUG, not needed) *)
  | LBL_TRY       (* a label at the start of a try block *)
  | LBL_CATCH     (* a label at the start of a catch block *)
  | LBL_FINALLY   (* a label at the start of a finally block *)
  | LBL_RERAISE   (* a label after the end of a finally block *)
  | LBL_END       (* a label after the end of a block (while or try) *)


(****************************
 *        CONSTANTS         *
 ****************************)

(** check if I'm on my local machine, which has 64 bit
 *  function returns. (why though ?)
 *)
let all_64bit = try Sys.getenv "USER" = "althonos" with Not_found -> false

(** (partial) list of functions that return a 64bit values
 *)
let std64 = ["malloc"; "calloc"; "kalloc"; "realloc"; "fopen"; "exit"]


(****************************
 *          UTILS           *
 ****************************)

(* like List.fold_left, but supports the indexes as well *)
let fold_left_index f a l =
    let rec list_folder l a i =
      match l with
        | []       -> a
        | hd :: tl -> list_folder tl (f a i hd) (i+1)
    in list_folder l a 0

(* create a locals list from a list of variable definition *)
let rec make_locals locator varlist offset base =
  match varlist with
    | [] -> base
    | CDECL (_, name) :: tail ->
        if
          List.exists (fun v -> match v with CDECL (_, n) -> n = name | _ -> false) tail
        then
          begin
            Error.warning (Some locator) ("duplicate variable declaration " ^ name);
            (make_locals locator tail (offset-8) base)
          end
        else
        (name, offset) :: (make_locals locator tail (offset-8) base)
    | CFUN (loc, _, _, _) :: tail ->
        Error.fatal (Some loc) "inner function declaration"

(* create a global string identifier from a locator *)
let make_label label_type ?comment (file, l1, c1, l2, c2) =
    (match label_type with
       | LBL_STR     -> "str" | LBL_THEN    -> "thn"
       | LBL_ELSE    -> "els" | LBL_FI      -> "fi"
       | LBL_DO      -> "do"  | LBL_END     -> "end"
       | LBL_IF      -> "if"  | LBL_WHILE   -> "whl"
       | LBL_TRY     -> "try" | LBL_FINALLY -> "fly"
       | LBL_CATCH   -> "cat" | LBL_RERAISE -> "rrs") ^
    (sprintf "%i_%i_%i_%i" l1 c1 l2 c2) ^
    (match comment with | None -> "" | Some c -> "_" ^ c)

(* get a list of the names of the functions declared in the file *)
let get_local_functions decl_list =
  List.fold_left (fun a d -> match d with CFUN (_, n, _, _) -> n :: a | _ -> a) [] decl_list

(* get a sublist of a list, i.e. list[i:j], Python style slicing *)
let sublist l i j =
  let rec sublist_aux l i j pos =
    match l with
      | [] -> []
      | hd :: tl -> (
          if i <= pos && pos < j
          then hd :: (sublist_aux tl i j (pos+1))
          else sublist_aux tl i j (pos+1)
        )
  in sublist_aux l i j 0

(* compare the decl so that the globals declarations are compiled first *)
let compare_decl i j = match (i, j) with
    | (CFUN _, CFUN _)   ->  0 | (CDECL _, CDECL _) ->  0
    | (CFUN _, CDECL _)  ->  1 | (CDECL _, CFUN _)  -> -1

(* push a list of registers in a possibly optimized way :
 *   push ["%rax", "%rbx", "%rcx"] --> "push %rax", "push %rbx", "push %rcx"
 *)
let pushall registers =
  let rec push_folder i registers =
    match registers with
      | [] -> ""
      | hd :: tl -> (sprintf "\tmovq\t%s, %i(%%rsp)\n" hd (i*8)) ^ (push_folder (i-1) tl)
  in
  match List.length registers with
    | 0 -> ""
    | 1 -> sprintf "\tpushq\t%s\n" (List.hd registers)
    | x -> (sprintf "\tsubq\t$%i, %%rsp\n" (x*8)) ^ (push_folder (x-1) registers)

(* pop a list of registers in a possibly optimized way :
 *   popall ["%rax", "%rbx", "%rcx"] --> "pop %rcx", "pop %rbx", "pop %rax"
 *)
let popall registers =
  let rec pop_folder i registers =
    match registers with
      | [] -> ""
      | hd :: tl -> (sprintf "\tmovq\t%i(%%rsp), %s\n" (i*8) hd) ^ (pop_folder (i-1) tl)
  in
  match List.length registers with
    | 0 -> ""
    | 1 -> sprintf "\tpopq\t%s\n" (List.hd registers)
    | x -> (pop_folder (x-1) registers) ^ (sprintf "\taddq\t$%i, %%rsp\n" (x*8))


(*************************
 *       ENVIRONMENT     *
 *************************)

let environ = object (self)

  val locals = []
  val globals = ref []
  val functions = []
  val exceptions = ref []
  val error_handler = (None, None)

  (* get the list of locals *)
  method get_locals =
    locals

  (* get an immutable view of the list of globals *)
  method get_globals =
    !globals

  (* get the list of functions *)
  method get_functions =
    functions

  (* get the local error handler *)
  method get_handler =
    error_handler

  (* return copy of the environment with the given functions *)
  method with_functions functions =
    {< functions = functions >}

  (* return a copy of the environment with the given error handler *)
  method with_handler handler =
    {< error_handler = handler >}

  (* return a copy of the environment with the given locals *)
  method with_locals locals =
    {< locals = locals >}

  (* add a global to the environment *)
  method add_global global =
    globals := global :: !(globals);

  (* add an exception to the environment *)
  method add_exception exc =
    begin
      exceptions := match !exceptions with
                      | [] -> [(exc, 1)]
                      | (_, i) :: tl -> (exc, i+1) :: !exceptions
    end

  (* add a string to the environment and return its id *)
  method add_string loc value =
    let id = make_label LBL_STR loc in
      begin self#add_global (id, (Some value)); id end

  (* get the location of a variable *)
  method get_location name =
    (* check if name refers to a local variable *)
    try
      let (_, offset) = List.find (fun (n, _) -> n = name) locals in
        Some (sprintf "%i(%%rbp)" offset)
    with Not_found ->
      (* check if name refers to a global variable *)
      try
        let (name, _) = List.find (fun (n, _) -> n = name) !globals in
          Some (sprintf "%s(%%rip)" name)
      with Not_found -> None

  (* get the identifier of a string from its value *)
  method get_string_id value =
    try
      let (name, _) = List.find (fun (n, v) -> v = Some value) !globals
        in Some name
    with Not_found -> None

  (* get the identifier of an exception from its name *)
  method get_exception_id exc =
    let (_, id) = List.find (fun (n, _) -> n = exc) !exceptions in
      id

  (* get the current local offset, i.e. the offset of the variable the farthest from %rbp *)
  method current_locals_offset =
    List.fold_left (fun a (_, offset) -> if a > offset then offset else a) 0 locals

  (* check it the given exception is already in the exceptions list or not *)
  method is_exception_known exc =
    try
      List.find (fun (n, _) -> n = exc) !exceptions; true
    with Not_found -> false

end




(****************************
 *         COMPILERS        *
 ****************************)

(* compile first level of depth declaration ~ functions are allowed *)
let rec compile_root_decl decl env =
  match decl with
    | CDECL (loc, name) ->
        env#add_global (name, None); ""
    | CFUN (loc, name, varlist, (codeloc, code)) ->
        let arg_count = List.length varlist in
        let rgstr_args = sublist varlist 0 6
        and stack_args = List.rev (sublist varlist 6 arg_count) in
        (* move the arguments from the registers to the stack *)
        let move_args_to_stack =
          fold_left_index (
            fun a i r -> a ^ (sprintf "\tmovq\t%%%s, -%i(%%rbp)\n" r ((i+1)*8))
          ) "" (sublist ["rdi"; "rsi"; "rdx"; "rcx"; "r8"; "r9"] 0 arg_count)
        in
        (* create a clean locals list from the argument list *)
        let new_env =
          let locals =
            (* arguments already on the stack *)             (* register arguments we put on the stack ourselves *)
            make_locals loc stack_args ((arg_count - 5) * 8) (make_locals loc rgstr_args (-8) [])
          in
          env#with_locals locals
        in
        (* label beginning of function *)
        name ^ ":\n" ^
        (* assembler junk *)
        (sprintf "\t.globl\t%s\n\t.type\t%s, @function\n" name name) ^
        (* store the stack context *)
        "\tpushq\t%rbp\n\tmovq\t%rsp, %rbp\n" ^
        (* reset the exceptions registers *)
        "\txorq\t%r10, %r10\n\txorq\t%r11, %r11\n" ^
        (* allocate space for registers parameters to be put on the stack *)
        (if arg_count > 0 then sprintf "\tsubq\t$%i, %%rsp\n" ((min arg_count 6)*8) else "") ^
        (* move the registers to the stack *)
        move_args_to_stack ^
        (* code of the function *)
        compile_loc_code (codeloc, code) new_env ^
        (* FIXME just in case add a leave / ret so that the func returns *)
        "\tleave\n\tret\n\n" ^
        (* assembler junk part 2 *)
        (sprintf "\t.size\t%s, .-%s\n" name name)


(* compile an expression ~ an expression puts its result into %rax *)
and compile_loc_expr (loc, expr) env =
  match expr with
    | VAR name -> (
        match env#get_location name with
          | Some location -> "\tmovq\t" ^ location ^ ", %rax\n"
          | None -> "\tmovq\t" ^ name ^ "(%rip), %rax\n"
      )
    | CST value ->
        (* store the constant modulo 2^32 *)
        sprintf "\tmovq\t$%i, %%rax\n" value
        (* (value land ((1 lsl 32)-1)) *)
    | STRING str ->
        sprintf "\tleaq\t%s(%%rip), %%rax\n" (
          match env#get_string_id str with
            | Some id -> id
            | None    -> env#add_string loc str
        )
    | SET_VAR (name, loc_expr') ->
        (* compute the new value of the variable into rax *)
        (compile_loc_expr loc_expr' env) ^ (
        (* set the variable to the given value *)
        match env#get_location name with
          | Some location -> sprintf "\tmovq\t%%rax, %s\n" location
          | None -> Error.fatal (Some loc) (sprintf "undefined variable '%s'" name)
        )
    | SET_ARRAY (name, key_expr, value_expr) ->
        (* save registers *)
        "\tpushq\t%rbx\n" ^
        (* compute the value *)
        (compile_loc_expr value_expr env) ^
        (* store the value on the stack *)
        "\tpushq\t%rax\n" ^
        (* compute the key *)
        (compile_loc_expr key_expr env) ^
        (* put the key in %rbx *)
        "\tmovq\t%rax, %rbx\n" ^ (
        (* get the offset of the array variable in memory *)
        match env#get_location name with
          | Some location -> sprintf "\tmovq\t%s, %%rax\n" location
          | None -> Error.fatal (Some loc) (sprintf "undefined array '%s'" name)
        ) ^
        (* move the new array value to both the memory and %rax *)
        "\tpopq\t(%rax, %rbx, 8)\n\tmovq\t-8(%rsp), %rax\n" ^
        (* restore %rbx to its initial value *)
        "\tpopq\t%rbx\n"
    | CALL (funcname, args) ->
        if not (List.mem funcname env#get_functions) then
          Error.warning (Some loc) (sprintf "assuming '%s' is a libc function" funcname);
        let arg_count = List.length args
        in
        (* get the registers to save depending on the number of arguments *)
        let registers_to_save arg_count =
          if arg_count > 4
            then sublist ["%rcx"; "%rdx"; "%rbx"; "%rdi"; "%rsi"; "%r8"; "%r9"] 0 (arg_count+1)
            else sublist ["%rcx"; "%rdx"; "%rbx"; "%rdi"; "%rsi"] 0 (arg_count+3)
        in
        (* put each argument at the right place; arglist starts with the last argument *)
        let rec fold_args arglist =
           let arg_registers = ["%rdi"; "%rsi"; "%rdx"; "%rcx"; "%r8"; "%r9"] in
           match arglist with
             | [] -> ""
             | loc_expr :: tl -> let rank = List.length tl in
                  (compile_loc_expr loc_expr env) ^ (
                    if rank > 5 then
                      "\tpushq\t%rax\n"
                    else
                      sprintf "\tmovq\t%%rax, %s\n" (List.nth arg_registers rank)
                  ) ^ (fold_args tl)
        in
        (* save the registers used to pass arguments and exceptions *)
        (pushall (["%r10"; "%r11"] @ (registers_to_save arg_count))) ^
        (* align the stack on 16 bytes *)
	       "\tmovq\t%rsp, %rbx\n\tandq\t$-16, %rsp\n" ^
        (if ((arg_count > 6) && (arg_count mod 2 != 0)) then "\tsubq\t$8, %rsp\n" else "") ^
        (* move the arguments to the right location *)
        (fold_args (List.rev args)) ^
        (* reset %rax for the function call *)
        "\txorq\t%rax, %rax\n" ^
        (* reset the exception pointer & exception value register *)
        "\txorq\t%r10, %r10\n\txorq\t%r11, %r11\n" ^
        (* call the function ~ result will be in %rax *)
        (sprintf "\tcall\t%s@PLT\n" funcname) ^
        (* restore the rsp to its state before calling *)
        "\tmovq\t%rbx, %rsp\n" ^
        (* restore the registers used to pass the arguments *)
        (popall (registers_to_save arg_count)) ^
        (* convert output of functions to 64 bits if it returns a 32 bit value *)
        (if all_64bit || (List.mem funcname (std64 @ env#get_functions)) then "" else "\tcltq\n") ^
        (* if the function is not a libc function, check for exceptions *)
        if List.mem funcname env#get_functions then
          (* check if the function raised an exception and reraise it if so *)
          (sprintf "\tcmpq\t$0, %%r10\n\tje\t.%s\n" (make_label LBL_END loc)) ^
          (* discard the stored %r10 and %r11 if we raised an exception *)
          "\taddq\t$16, %rsp\n" ^ (
          (* reach the closest exception handler or return if there is none *)
          match env#get_handler with
            | (None, None)  -> "\tleave\n\tret\n"
            | (Some lbl, _) -> sprintf "\tjmp\t.%s\n" lbl
            | (_, Some lbl) -> sprintf "\tjmp\t.%s\n" lbl
          ) ^
          (* add a label after the "reraise" logic block *)
          (sprintf ".%s:\n" (make_label LBL_END loc)) ^
          (* simply pop %r10 and %r11 if we didn't *)
          (popall ["%r10"; "%r11"])
        else
          (* just reset the exception registers, this is a libc function *)
          (popall ["%r10"; "%r11"])
    | OP1 (op, (loc', expr')) ->
        (* the adress of the modified variable *)
        let location =
          match expr' with
            | VAR name -> (
                match env#get_location name with
                  | Some location -> location
                  | None -> Error.fatal (Some loc) "cannot increment a libc constant";
              )
            | OP2 (S_INDEX, _, _) -> "(%rdx, %rbx, 8)"
            | _ -> "%rax"
        in
        (* the registers to save *)
        let registers =
          match expr' with
            | OP2 (_, _, _) -> ["%rbx"; "%rcx"; "%rdx"]
            | _                   -> ["%rbx"; "%rcx"]
        in
        (* save registers, and load effective adress of array elements if needed *)
        (pushall registers) ^ (
        match expr' with
          | VAR _ -> ""
          (* for a S_INDEX op, first of all load the expected adress in rax[rdx] *)
          | OP2 (S_INDEX, loc_expr1, loc_expr2) -> (
              (compile_loc_expr loc_expr2 env) ^
              "\tpushq\t%rax\n" ^
              (compile_loc_expr loc_expr1 env) ^
              "\tmovq\t%rax, %rdx\n\tpopq\t%rbx\n"
            )
          | _ -> compile_loc_expr (loc', expr') env
        (* put the old value in both rax and rcx *)
        ) ^ (sprintf "\tmovq\t%s, %%rax\n\tmovq\t%%rax, %%rcx\n" location) ^ (
        (* compute the expected operation *)
        match op with
          | M_PRE_INC  -> sprintf "\tincq\t%%rax\n\tmovq\t%%rax, %s\n" location
          | M_POST_INC -> sprintf "\tincq\t%%rax\n\tmovq\t%%rax, %s\n\tmovq\t%%rcx, %%rax\n" location
          | M_PRE_DEC  -> sprintf "\tdecq\t%%rax\n\tmovq\t%%rax, %s\n" location
          | M_POST_DEC -> sprintf "\tdecq\t%%rax\n\tmovq\t%%rax, %s\n\tmovq\t%%rcx, %%rax\n" location
          | M_MINUS    -> "\tneg\t%rax\n"
          | M_NOT      -> "\tnot\t%rax\n"
        (* restore registers *)
        ) ^ (popall registers)
    | OP2 (op, loc_expr1, loc_expr2) ->
        let registers =
          match op with S_MOD | S_INDEX | S_DIV | S_MUL -> ["%rdx"] | _ -> []
        in
        (* save the additional registers *)
        (pushall registers) ^
        (* compute the right expression *)
        (compile_loc_expr loc_expr2 env) ^
        (* store result on the stack *)
        "\tpushq\t%rax\n" ^
        (* compute the left expression *)
        (compile_loc_expr loc_expr1 env) ^
        (* pop result in %rdx only if needed ! *)
        (if op = S_INDEX then "\tpopq\t%rdx\n" else "\taddq\t$8, %rsp\n") ^ (
        (* compute the appropriate binary operation *)
        match op with
          | S_ADD   -> "\taddq\t-8(%rsp), %rax\n"
          | S_DIV   -> "\tcqto\n\tidivq\t-8(%rsp)\n"
          | S_INDEX -> "\tmovq\t(%rax, %rdx, 8), %rax\n"
          | S_MOD   -> "\tcqto\n\tidivq\t-8(%rsp)\n\tmovq\t%rdx, %rax\n"
          | S_MUL   -> "\timulq\t-8(%rsp)\n"
          | S_SUB   -> "\tsubq\t-8(%rsp), %rax\n"
        (* modulo 2^32 if needed *)
        ) ^ (*(if op = S_INDEX then "" else "\tmovsxd\t%eax, %rax\n") ^*)
        (* restore the saved registers *)
        (popall registers)
    | CMP (op, loc_expr1, loc_expr2) ->
        (* compile the right expression *)
        (compile_loc_expr loc_expr2 env) ^
        (* store the result *)
        "\tpushq\t%rax\n" ^
        (* compile the left expression *)
        (compile_loc_expr loc_expr1 env) ^
        (* compare the two values *)
        "\tcmpq\t(%rsp), %rax\n" ^ (
        (* set %al to 1 depending on the comparison *)
        match op with
          | C_EQ -> "\tsete\t%al\n"
          | C_LE -> "\tsetle\t%al\n"
          | C_LT -> "\tsetl\t%al\n"
        (* extend the lower byte and discard the result of expr2 *)
        ) ^ "\tmovzbq\t%al, %rax\n\taddq\t$8, %rsp\n"
    | ESEQ le_list ->
        List.fold_left (fun a le -> a ^ (compile_loc_expr le env)) "" le_list
    | EIF (test, on_true, on_false) ->
      let if_label = make_label LBL_IF ~comment:"inline" loc
      and then_label = make_label LBL_THEN ~comment:"inline" loc
      and else_label = make_label LBL_ELSE ~comment:"inline" loc
      and fi_label = make_label LBL_FI ~comment:"inline" loc in
        (* add a label at the start of the block *)
        (sprintf ".%s:\n" if_label) ^
        (* compile the test *)
        (compile_loc_expr test env) ^
        (* add a label at the start of the if block *)
        (sprintf ".%s:\n" then_label) ^
        (* check the truth value of the test and jump to the else if false *)
        (sprintf "\tcmpq\t$0, %%rax\n\tje\t.%s\n" else_label) ^
        (* compile the true expression *)
        (compile_loc_expr on_true env) ^
        (* at the end of the if branch jump past the else branch *)
        (sprintf "\tjmp\t.%s\n" fi_label) ^
        (* add a label at the start of the else branch *)
        (sprintf ".%s:\n" else_label) ^
        (* compile the code of the else branch *)
        (compile_loc_expr on_false env) ^
        (* add a label at the end of the if/else block *)
        (sprintf ".%s:\n" fi_label)


(* compile a code block *)
and compile_loc_code (loc, code) env =
  match code with
    | CBLOCK (varlist, lcodelist) ->
        (* the block locals are the parent locals + new vars *)
        let new_env =
          let start_offset = env#current_locals_offset - 8 in
            let new_locals = (make_locals loc varlist start_offset env#get_locals) in
              env#with_locals new_locals
        in
        (* concatenate the compiled code of every instruction *)
        let compiled_code =
          List.fold_left (fun a lc -> a ^ compile_loc_code lc new_env) "" lcodelist
        in
        (* compute the size to allocate on the stack *)
        let allocation_size =
          ((List.length new_env#get_locals) - (List.length env#get_locals)) * 8
        in
        (sprintf "\tsubq\t$%i, %%rsp\n" allocation_size) ^
        compiled_code ^
        (sprintf "\taddq\t$%i, %%rsp\n" allocation_size)
    | CEXPR loc_expr ->
        compile_loc_expr loc_expr env
    | CIF ((loc_test, code_test), (loc_true, code_true), (loc_false, code_false)) ->
      let if_label = make_label LBL_IF loc
      and then_label = make_label LBL_THEN loc
      and else_label = make_label LBL_ELSE loc
      and fi_label = make_label LBL_FI loc in
        (* add a comment at the start of the if *)
        (sprintf "#%s:\n" if_label) ^
        (* compile the test *)
        (compile_loc_expr (loc_test, code_test) env) ^
        (* print a comment at the start of the if branch *)
        (sprintf "#%s:\n" then_label) ^
        (* check the truth value of the test and jump to the else if false *)
        (sprintf "\tcmpq\t$0, %%rax\n\tje\t.%s\n" else_label) ^
        (* compile the code of the if branch *)
        (compile_loc_code (loc_true, code_true) env) ^
        (* at the end of the if branch jump past the else branch *)
        (sprintf "\tjmp\t.%s\n" fi_label) ^
        (* add a label at the start of the else branch *)
        (sprintf ".%s:\n" else_label) ^
        (* compile the code of the else branch *)
        (compile_loc_code (loc_false, code_false) env) ^
        (* add a label at the end of the if/else block *)
        (sprintf ".%s:\n" fi_label)
    | CWHILE ((loc_test, code_test), (loc_block, code_block)) ->
      let while_label = make_label LBL_WHILE loc
      and do_label = make_label LBL_DO loc
      and end_label = make_label LBL_END loc in
        (* add a label at the beginning of the while block *)
        (sprintf ".%s:\n" while_label) ^
        (* compile the test *)
        (compile_loc_expr (loc_test, code_test) env) ^
        (* evaluate the test and jump after the loop if false *)
        (sprintf "\tcmpq\t$0, %%rax\n\tje\t.%s\n" end_label) ^
        (* add a comment at the beginning of the loop code *)
        (sprintf "#%s:\n" do_label) ^
        (* compile the code of the loop *)
        (compile_loc_code (loc_block, code_block) env) ^
        (* jump back to the beginning of the loop *)
        (sprintf "\tjmp\t.%s\n" while_label) ^
        (* add a label after the end of the loop *)
        (sprintf ".%s:\n" end_label)
    | CRETURN None -> (
        match env#get_handler with
          | (_, None)     ->
              (* out of a try block OR try block without finally: just return with no exception *)
              "\txorq\t%r10, %r10\n\tleave\n\tret\n"
          | (_, Some lbl) ->
              (* in a try block with a finally: execute the finally, return after it *)
              "\tmovq\t$-1, %r10\n\tmovq\t%rax, %r11\n\tmovq\t$1, %rax\n" ^
              (sprintf "\tjmp\t.%s\n" lbl)
        )
    | CRETURN Some loc_expr ->
        (* compile the return value into %rax *)
        (compile_loc_expr loc_expr env) ^
        (* return nothing since now there is a proper return value in %rax *)
        (compile_loc_code (loc, CRETURN None) env)
    | CTHROW (exc, loc_expr) -> (
        (* add the exception to the exception list, so we can allocate an adress for it *)
        if not (env#is_exception_known exc) then env#add_exception exc;
        (* compile the exception "return value" *)
        (compile_loc_expr loc_expr env) ^
        (* move the exception adress to %r10, the exception value to %r11 *)
        "\tmovq\t%rax, %r11\n" ^
        (* move the exception unique id to %r10 *)
        (sprintf "\tmovq\t$%i, %%r10\n" (env#get_exception_id exc)) ^
        (* (sprintf "\tleaq\t._exn%s(%%rip), %%r10\n" name) ^ ( *)
        (* test whether there is a local exception handler, or if we return *)
        match env#get_handler with
          (* block without a catch / finally -> return with the exception *)
          | (None, None)  -> "\tmovq\t$1, %rax\n\tleave\n\tret\n"
          (* block with at least one catch -> jump to it *)
          | (Some lbl, _) -> sprintf "\tjmp\t.%s\n" lbl
          (* block with only a finally -> jump to it *)
          | (_, Some lbl) -> sprintf "\tjmp\t.%s\n" lbl
        )
    | CTRY ((loc_try, code_try), handlers, maybe_finally) -> (
        let finally_label = make_label LBL_FINALLY loc
        and reraise_label = make_label LBL_RERAISE loc
        and end_label     = make_label LBL_END loc in
        (* the first error handler label / finally_label *)
        let handler =
          match handlers with
            | [] ->
                (None, Some finally_label)
            | (e, _, _) :: tl -> (
                if not (env#is_exception_known e) then env#add_exception e;
                (Some (make_label LBL_CATCH ~comment:e loc), Some finally_label)
              )
        in
        let new_env =
          (* "FAKE" variables to have the compiler allocate space to store %r10 and %r11
             values before entering the try block. This avoids messing with the top of the
             stack. No name collision since dot-prefixed are invalid syntax. *)
          let mock_vars = [CDECL(loc, ".r10"); CDECL(loc, ".r11")] in
            env#with_locals (make_locals loc mock_vars (env#current_locals_offset - 8) env#get_locals)
        in

        (* save %r10 and %r11 in a %rbp-relative location *)
        ("\tsubq\t$16, %rsp\n") ^
        (sprintf "\tmovq\t%%r10, %i(%%rbp)\n" (env#current_locals_offset - 8)) ^
        (sprintf "\tmovq\t%%r11, %i(%%rbp)\n" (env#current_locals_offset - 16)) ^
        (* try code is now in an exception-free context *)
        ("\txorq\t%r10, %r10\n\txorq\t%r11, %r11\n") ^

        (* add a comment beginning of try block *)
        (sprintf "#%s:\n" (make_label LBL_TRY loc)) ^
        (* compile the try code / we now have a new closest exception handler *)
        (compile_loc_code (loc_try, code_try) (new_env#with_handler handler)) ^
        (* end of try block / jump to the finally on no error *)
        (sprintf "\tjmp\t.%s\n" finally_label) ^
        (* beginning of catch blocks *)
        (compile_handlers loc handlers finally_label new_env) ^
        (* end of catch blocks / beginning of finally block *)
        (sprintf ".%s:\n" finally_label) ^

        (* compile the finally code if any *)
        (match maybe_finally with | None -> "" | Some lc -> compile_loc_code lc new_env) ^

        (* check if the previous code (try/catch/finally) raised an exception *)
        "\tcmpq\t$0, %r10\n" ^
        (* if it didn't then restore the original exception context *)
        (sprintf "\tjne\t.%s\n" (make_label LBL_FINALLY ~comment:"end" loc)) ^
        (sprintf "\tmovq\t%i(%%rbp), %%r10\n" (env#current_locals_offset - 8)) ^
        (sprintf "\tmovq\t%i(%%rbp), %%r11\n" (env#current_locals_offset - 16)) ^
        (sprintf ".%s:\n" (make_label LBL_FINALLY ~comment:"end" loc)) ^
        (* free allocated stack *)
        ("\taddq\t$16, %rsp\n") ^

        (* end of finally block: decide what to do next *)
        match env#get_handler with
          (* we're in a try block with at least one handler -> jump to it only on error *)
          | (Some c, Some f) -> (sprintf "\tcmpq\t$0, %%r10\n\tjle\t.%s\n\tjmp\t.%s\n" f c)
          (* we're in a try block with only finally -> jump to it *)
          | (None, Some lbl) -> sprintf "\tjmp\t.%s\n" lbl
          (* we're not in a try block -> return the right value or exception *)
          | (None, None)  -> (
              (* put 1 in %rax in case we're reraising *)
              "\tmovq\t$1, %rax\n" ^
              (* check what to do after the finally: reraise / return / do nothing *)
              (sprintf "\tcmpq\t$0, %%r10\n\tjg\t.%s\n\tje\t.%s\n" reraise_label end_label) ^
              (* if we return: move the exception value to %rax *)
              "\tmovq\t%r11, %rax\n\txorq\t%r10, %r10\n" ^
              (* if we're reraising: return *)
              (sprintf ".%s:\n\tleave\n\tret\n" reraise_label) ^
              (* if were doing nothing: continue code execution *)
              (sprintf ".%s:\n" end_label)
            )
      )


(* compile an exception handling block *)
and compile_handlers loc handlers finally_label env =
  let rec compile_handlers_aux (handlers_left: (string * string * loc_code) list) =
    match handlers_left with
      | [] -> ""
      | (e, n, lc) :: tl -> (
          let var_location = env#current_locals_offset - 8 in
          let new_env = env#with_locals ((n, var_location) :: env#get_locals) in
          if not (env#is_exception_known e) then env#add_exception e;
          (* add a label for the handler *)
          (sprintf ".%s:\n" (make_label LBL_CATCH ~comment:e loc)) ^
          (* check if the handler is for this exception *)
          (sprintf "\tcmpq\t$%i, %%r10\n" (env#get_exception_id e)) ^
          (* if not, jump to the next handler, or finally if there's not *)
          (sprintf "\tjne\t.%s\n" (
            match tl with
              | [] -> finally_label
              | (next_e, _, _) :: tl -> (make_label LBL_CATCH ~comment:next_e loc)
          (* allocate space for exception value *)
          )) ^ "\tsubq\t$8, %rsp\n" ^(*("\tsubq\t$256, %rsp\n") ^*)
          (* move the exception value to the stack *)
          (sprintf "\tmovq\t%%r11, %i(%%rbp)\n" var_location) ^
          (* if we found the right handler, delete the exception adress *)
          "\txorq\t%r10, %r10\n" ^
          (* compile the code of the handler *)
          (compile_loc_code lc new_env) ^
          (* free the allocated memory *)
          ("\taddq\t$8, %rsp\n") ^
          (* jump to the finally once the handler is done *)
          (sprintf "\tjmp\t.%s\n" finally_label) ^
          (* keep on compiling handlers *)
          (compile_handlers_aux tl)
        )
  in
  (compile_handlers_aux handlers)


(* compile the data section from the environment globals *)
and compile_data env =
    if env#get_globals = [] then "" else
      List.fold_left (fun acc global ->
        match global with
          (* global is an integer variable *)
          | (id, None) -> acc ^ (sprintf "\t.comm %s,8,8\n" id)
          (* global is a constant string *)
          | (id, Some str) -> acc ^ (
              sprintf "%s:\n\t.string\t\"%s\"\n\t.align\t8\n" id (String.escaped str)
            )
      ) ".data\n" env#get_globals


(****************************
 *          MAIN            *
 ****************************)

(* compile a list of declaration *)
let compile out decl_list =
  (* sort the declaration list to compile the globals first: this way, *
   * we can crash the compiler when assigning to an unknown variable   *)
  let decl_list = List.sort compare_decl decl_list in
  (* create the environment from the function list *)
  let env = environ#with_functions (get_local_functions decl_list) in
  (* first, we compile the functions (so that strings are added to the globals )*)
  let funcdefs = List.fold_left (fun a dl -> a ^ (compile_root_decl dl env)) ".text\n" decl_list in
  (* then the data section *)
  let data = compile_data env in
  (* then we assemble everything *)
  let code = data ^ "\n" ^ funcdefs ^ "\t.ident	\"MCC: (Martin Larralde) v0.1.0\"\n" in
  (* and finally we write it to the given pipe! *)
  fprintf out "%s" code
